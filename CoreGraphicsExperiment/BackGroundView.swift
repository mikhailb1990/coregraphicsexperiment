//
//  BackGroundView.swift
//  CoreGraphicsExperiment
//
//  Created by Mikhail Bhuta on 7/2/16.
//  Copyright © 2016 Mikhail Bhuta. All rights reserved.
//

import UIKit

@IBDesignable class BackGroundView: UIView {
    
    @IBInspectable var lightColor: UIColor = UIColor.orangeColor()
    @IBInspectable var darkColor: UIColor = UIColor.yellowColor()
    @IBInspectable var patternSize: CGFloat = 200

    override func drawRect(rect: CGRect) {
        // Drawing code
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSetFillColorWithColor(context, darkColor.CGColor)
        
        CGContextFillRect(context, rect)
        
        let drawSize = CGSize(width: patternSize, height: patternSize)
        
        UIGraphicsBeginImageContextWithOptions(drawSize, true, 0.0)
        let drawingContext = UIGraphicsGetCurrentContext()
        darkColor.setFill()
        CGContextFillRect(drawingContext, CGRect(x: 0, y: 0, width: drawSize.width, height: drawSize.height))
        
        let trianglePath = UIBezierPath()
        
        // Triangle 1
        trianglePath.moveToPoint(CGPoint(x: drawSize.width/2, y: 0))
        trianglePath.addLineToPoint(CGPoint(x: 0, y: drawSize.height/2))
        trianglePath.addLineToPoint(CGPoint(x: drawSize.width, y: drawSize.height/2))
        
        // Triangle 2
        trianglePath.moveToPoint(CGPoint(x: 0, y: drawSize.height/2))
        trianglePath.addLineToPoint(CGPoint(x: drawSize.width/2, y: drawSize.height))
        trianglePath.addLineToPoint(CGPoint(x: 0, y: drawSize.height))
        
        // Triangle 3
        trianglePath.moveToPoint(CGPoint(x: drawSize.width, y: drawSize.height/2))
        trianglePath.addLineToPoint(CGPoint(x:drawSize.width/2, y:drawSize.height))
        trianglePath.addLineToPoint(CGPoint(x: drawSize.width, y: drawSize.height))
        
        lightColor.setFill()
        trianglePath.fill()
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        UIColor(patternImage: image).setFill()
        CGContextFillRect(context, rect)
    }
    

}
