//
//  CounterView.swift
//  CoreGraphicsExperiment
//
//  Created by Mikhail Bhuta on 7/1/16.
//  Copyright © 2016 Mikhail Bhuta. All rights reserved.
//

import UIKit

let NumGlasses = 8
let π:CGFloat = CGFloat(M_PI)

@IBDesignable class CounterView: UIView {
    
    var startAngle: CGFloat!
    var arcLengthPerGlass: CGFloat!
    
    @IBInspectable var counter: Int = 5
    {
        didSet {
            if counter <= NumGlasses
            {
                setNeedsDisplay()
            }
        }
    }
    @IBInspectable var counterColor: UIColor = UIColor.orangeColor()
    @IBInspectable var outlineColor : UIColor = UIColor.blueColor()

    override func drawRect(rect: CGRect) {
        // Drawing code
        drawCounter()
        
        // DRAW MARKERS FOR COUNTER
        let context = UIGraphicsGetCurrentContext()
      
        // Save original state
        CGContextSaveGState(context)
        outlineColor.setFill()
        
        let markerWidth:CGFloat = 5
        let markerSize:CGFloat = 10
        
        // the marker rect positioned at the top left
        let markerPath = UIBezierPath(rect: CGRect(x: -markerWidth/2, y: 0, width: markerWidth, height: markerSize))
        
        // move top left corner of context to the previous center position
        CGContextTranslateCTM(context, rect.width/2, rect.height/2)
        
        for i in 1...NumGlasses {
            // Save the centered context
            CGContextSaveGState(context)
            
            let angle = arcLengthPerGlass * CGFloat(i) + startAngle - π/2
            CGContextRotateCTM(context, angle)
            CGContextTranslateCTM(context, 0, rect.height/2 - markerSize)
            
            markerPath.fill()
            
            CGContextRestoreGState(context)
        }
        
        CGContextRestoreGState(context)
    }

    func drawCounter()
    {
        let center = CGPoint(x: bounds.width/2, y: bounds.height/2)
        let diameter: CGFloat = max(bounds.width, bounds.height)
        let arcWidth: CGFloat = 76
        let arcDegrees: CGFloat = 270
        
        let arcRadians = arcDegrees * π/180
        
        startAngle = 3 * π/4
        let endAngle: CGFloat = startAngle + arcRadians - 2 * π
        
        let path = UIBezierPath(arcCenter: center, radius: diameter/2 - arcWidth/2, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        path.lineWidth = arcWidth
        counterColor.setStroke()
        path.stroke()
        
        let angleDifference: CGFloat = 2 * π - startAngle + endAngle
        arcLengthPerGlass = angleDifference/CGFloat(NumGlasses)
        let outlineEndAngle = arcLengthPerGlass * CGFloat(counter) + startAngle
        
        let outlinePath = UIBezierPath(arcCenter: center, radius: diameter/2 - 2.5, startAngle: startAngle, endAngle: outlineEndAngle, clockwise: true)
        
        outlinePath.addArcWithCenter(center, radius: diameter/2 - arcWidth + 2.5, startAngle: outlineEndAngle, endAngle: startAngle, clockwise: false)
        
        outlinePath.closePath()
        outlineColor.setStroke()
        outlinePath.lineWidth = 5
        outlinePath.stroke()
    }
    
}
