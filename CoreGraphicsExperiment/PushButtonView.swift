//
//  PushButtonView.swift
//  CoreGraphicsExperiment
//
//  Created by Mikhail Bhuta on 6/30/16.
//  Copyright © 2016 Mikhail Bhuta. All rights reserved.
//

import UIKit

@IBDesignable class PushButtonView: UIButton {
    
    @IBInspectable var fillColor: UIColor = UIColor.greenColor()
    @IBInspectable var isAddButton: Bool = true

    override func drawRect(rect: CGRect) {
        // Drawing code
        let path = UIBezierPath(ovalInRect: rect)
        fillColor.setFill()
        path.fill()
        drawPlusSign()
    }
    
    func drawPlusSign()
    {
        let plusHeight: CGFloat = 3
        let plusWidth:CGFloat = min(self.bounds.width, self.bounds.height) * 0.45
        
        // set stroke color
        UIColor.whiteColor().setStroke()

        
        // creat horizontal path
        let plusPath = UIBezierPath()
        plusPath.lineWidth = plusHeight
        
        // move path to initial starting point and create line
        plusPath.moveToPoint(CGPoint(x: bounds.width/2 - plusWidth/2 + 0.5, y: bounds.height/2 + 0.5))
        plusPath.addLineToPoint(CGPoint(x: bounds.width/2 + plusWidth/2 + 0.5, y: bounds.height/2 + 0.5))
        
        if isAddButton
        {
            // draw vertical line
            plusPath.moveToPoint(CGPoint(x: bounds.width/2 + 0.5 ,y: bounds.height/2 - plusWidth/2 + 0.5))
            plusPath.addLineToPoint(CGPoint(x: bounds.width/2 + 0.5 ,y: bounds.height/2 + plusWidth/2 + 0.5))
        
        }
        
        plusPath.stroke()
    }

}