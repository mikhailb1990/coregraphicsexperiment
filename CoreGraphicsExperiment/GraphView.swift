//
//  GraphView.swift
//  CoreGraphicsExperiment
//
//  Created by Mikhail Bhuta on 7/1/16.
//  Copyright © 2016 Mikhail Bhuta. All rights reserved.
//

import UIKit

@IBDesignable class GraphView: UIView {
    
    var graphPoints = [4, 2, 6, 4, 5, 8, 3]
    
    @IBInspectable var startColor: UIColor = UIColor.redColor()
    @IBInspectable var endColor: UIColor = UIColor.greenColor()

    override func drawRect(rect: CGRect) {
        
        let width = rect.width
        let height = rect.height
        
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 15)
        path.addClip()
        
        // Draw the gradient color
        let context = UIGraphicsGetCurrentContext()
        drawGradient(context, startPoint: CGPoint.zero, endPoint: CGPoint(x: 0, y: self.bounds.height))
        
        // Create the graph
        let graphPath = UIBezierPath()
        createGraph(graphPath, width: width, height: height, context: context)
        
        //graphPath.stroke()
        
    }
    
    func drawGradient(context: CGContext?, startPoint: CGPoint, endPoint: CGPoint)
    {
        let colors = [startColor.CGColor, endColor.CGColor]
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let colorLocations:[CGFloat] = [0.0, 1.0]
        
        let gradient = CGGradientCreateWithColors(colorSpace, colors, colorLocations)
        CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, CGGradientDrawingOptions(rawValue: 0))
    }

    func createGraph(graphPath: UIBezierPath, width: CGFloat, height: CGFloat, context: CGContext?)
    {
        // Calculate xpoint of graph
        let margin:CGFloat = 25.0
        let columnXPoint = { (column:Int) -> CGFloat in
            
            let spacer = (width - margin * 2) / CGFloat((self.graphPoints.count - 1))
            var x:CGFloat = CGFloat(column) * spacer
            x += margin
            return x
        }
        
        // Calculate ypoint of graph
        let topBorder: CGFloat = 60
        let bottomBorder: CGFloat = 50
        let graphHeight = height - topBorder - bottomBorder
        let maxVal = graphPoints.maxElement()
        let columnYPoint = { (graphPoint: Int) -> CGFloat in
            
            var y: CGFloat = CGFloat(graphPoint) / CGFloat(maxVal!) * graphHeight
            y = graphHeight + topBorder - y
            return y
            
        }
        
        // Draw the line graph
        UIColor.whiteColor().setStroke()
        UIColor.whiteColor().setFill()
        
        graphPath.moveToPoint(CGPoint(x: columnXPoint(0), y: columnYPoint(graphPoints[0])))
        
        for i in 1..<graphPoints.count
        {
            let nextPoint = CGPoint(x: columnXPoint(i), y: columnYPoint(graphPoints[i]))
            graphPath.addLineToPoint(nextPoint)
        }
        
        CGContextSaveGState(context)
        
        let clippingPath = graphPath.copy() as! UIBezierPath
        clippingPath.addLineToPoint(CGPoint(x: columnXPoint(graphPoints.count - 1), y: height))
        clippingPath.addLineToPoint(CGPoint(x: columnXPoint(0), y: height))
        clippingPath.closePath()
        clippingPath.addClip()
        
        let highestYPoint = columnYPoint(maxVal!)
        let startPoint = CGPoint(x: margin, y: highestYPoint)
        let endPoint = CGPoint(x: margin, y: self.bounds.height)
        drawGradient(context, startPoint: startPoint, endPoint: endPoint)
        
        CGContextRestoreGState(context)
        
        graphPath.lineWidth = 2
        graphPath.stroke()
        
        //Draw the circles on top of graph stroke
        for i in 0..<graphPoints.count {
            var point = CGPoint(x:columnXPoint(i), y:columnYPoint(graphPoints[i]))
            point.x -= 5.0/2
            point.y -= 5.0/2
            
            let circle = UIBezierPath(ovalInRect: CGRect(origin: point, size: CGSize(width: 5.0, height: 5.0)))
            circle.fill()
        }
        
        let linePath = UIBezierPath()
        linePath.moveToPoint(CGPoint(x: margin, y: topBorder))
        linePath.addLineToPoint(CGPoint(x: width - margin, y: topBorder))
        
        linePath.moveToPoint(CGPoint(x: margin, y: graphHeight/2 + topBorder))
        linePath.addLineToPoint(CGPoint(x: width - margin, y: graphHeight/2 + topBorder))
        
        linePath.moveToPoint(CGPoint(x:margin, y:height - bottomBorder))
        linePath.addLineToPoint(CGPoint(x:width - margin, y:height - bottomBorder))
        
        let color = UIColor(white: 1.0, alpha: 0.3)
        color.setStroke()
        
        linePath.lineWidth = 1.0
        linePath.stroke()
    }
}
